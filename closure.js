function anyfunc(){
    var name = 'John';
    function sayName(){
        console.log(name);   //because of closure this function can access the 'name' varriable even after its execution 
    };
    return sayName;
};
var logName = anyfunc();
logName();


function addition(x){
    return function addNum(y){
        console.log(x + y);
    };
}
var add5 = addition(5);
add5(3);


var add10 = addition(10)(5);