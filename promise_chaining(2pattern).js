// Let suppose there is an API call and that API will return a data only one time or only in first call
// after that there will be No data coming from that API just a resovled/rejected promise will come
// but you have to use that data again and again in promise chain




// Option 1. You will follow chaining *towards down pattern* and pass that 'data' to a global varriable so that it will accessible to all chaining methods


// let orders;             // varriable which is access through out all code
// let showOrders = (timer,work)=>{    
//     return new Promise((resolve,reject)=>{
//         setTimeout(()=>{
//             // suppose this 'data' will be coming from API for one time  
//             // after that it will never come and will be deleted from here
//             let data = [
//                 {
//                     orderName: 'mobile Phone',
//                     orderPrice: 12000,
//                     address: 'ludhiana',
//                     orderStatus: 'Shipped',
//                 },
//                 {
//                     orderName: 'Laptop',
//                     orderPrice: 22000,
//                     address: 'ludhiana',
//                     orderStatus: 'Out for Delivery',
//                 }
//             ];
//             try{
//                 if(work === "getData"){
//                     resolve(data);
//                 }
//                 else{
//                     resolve(work());
//                 }

//             }
//             catch(error){
//                 reject(console.log('Data not Found'));
//             };
//         },timer);
//     });
// };

// showOrders(2000, "getData")
// .then((data)=>{
//     orders = data;

//     return showOrders(2000, ()=>console.log(orders));
// })
// .then(()=>{
//     return showOrders(2000,()=>console.log(`You Have ${orders.length} orders`));
// })
// .then(()=>{
//     // lets make an error inside any then method-----this will only tell us that this particular then method is causing an error
//     // return showOrders(2000, new Error('Custom error'));

//     return showOrders(2000,()=>console.log(`Name of the first Product: ${orders[0].orderName}`));
// })
// .then(()=>{
//     return showOrders(1000,()=>console.log(`Order Status: ${orders[0].orderStatus}`));
// })
// .then(()=>{
//     return showOrders(2000,()=>console.log(`Name of the second Product: ${orders[1].orderName}`));
// })
// .catch(()=>console.log("Error Occured inside any call"));


// this pattern has its own benifits like--it will make a code more cleaner and understandable
// drawbacks-- 1.varriable 'orders' is accessible through out whole code so its value can be rewrite
//2.if there is an error caused by any .then method that will trigger catch which only one for all methods which cant tell that which .then method is causing error





// ---------------------------------------------------------------------------------------------------------------






// Option 2.Using chaining *towards right pattern* and pass that 'data' to an local varriable which accessiable inside all .then methods not globally


let showOrders = (timer,work)=>{
    
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            // suppose this 'data' will be coming from API for one time  
            // after that it will never come and will be deleted from here
            let data = [
                {
                    orderName: 'mobile Phone',
                    orderPrice: 12000,
                    address: 'ludhiana',
                    orderStatus: 'Shipped',
                },
                {
                    orderName: 'Laptop',
                    orderPrice: 22000,
                    address: 'ludhiana',
                    orderStatus: 'Out for Delivery',
                }
            ];
            try{
                if(work === "getData"){
                    resolve(data);
                }
                else{
                    resolve(work());
                }

            }
            catch(error){
                reject(console.log('Data not Found'));
            };
        },timer);
    });
};

showOrders(2000, "getData")
.then((data)=>{
    let orders = data;
    console.log('orders');

    showOrders(2000, ()=>console.log(orders))
    .then(()=>{
        showOrders(2000,()=>console.log(`You Have ${orders.length} orders`))
        .then(()=>{

            // lets make an error inside any then function----this will show the exact place or method causing an error
            // showOrders(2000, new Error('Custom Error'))

            showOrders(2000,()=>console.log(`Name of the first Product: ${orders[0].orderName}`))
            .then(()=>{
                showOrders(1000,()=>console.log(`Order Status: ${orders[0].orderStatus}`))
                .then(()=>{
                    showOrders(2000,()=>console.log(`Name of the second Product: ${orders[1].orderName}`)) 
                })
                .catch(()=>console.log('Error Occured -4'))
            })
            .catch(()=>console.log('Error Occured -3'))
        })
        .catch(()=>console.log('Error Occured -2'))
    })
    .catch(()=>console.log('Error Occured -1'))
});

// this pattern will show that 'orders' data is accessible to each and every nested .then method
// and no one will be able to change its value as it is accessible only in .then methods scope
// .catch methods are different for each and every .then which will tell the main cause of error

// drawbacks---as this leads to pyramid of doom like (callback pyramid) so code wont be understandable