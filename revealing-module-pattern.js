// without using revealing module pattern
var sayName2 = {
    name: 'Jack',
    logName: function(){console.log(this.name)}
};
sayName2.name = 'Kunwar';   // here anyone can change any property/value of varriables;
sayName2.logName();



// Using revealing Module pattern 
// [you can return only those variables,func anything which you want to reveal outside this sayName ]
var sayName = (function(){
    var name = 'Matt';
    function logName(){
        console.log(name);
    };
    return{
        logName: logName
    }
})();

sayName.name = 'john'       //can't rewrite the 'name' variable inside 'sayName' becoz this pattern wont declare that 'name' in global scope instead of sayName local scope
console.log(sayName);
sayName.logName();



var people = (function(){
    var people = ['John'];
    function addPerson(name){
        people.push(name);
    };
    function logPeople(){
        people.forEach(person => console.log(person));
    };

    return{
        addPerson: addPerson,
        logPeople: logPeople
    };
})();
people.addPerson('Kunwar');
people.logPeople();