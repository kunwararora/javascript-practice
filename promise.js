class Student{
    constructor(name, subject){
        this.name = name;
        this.subject = subject;
    };
};

let classArray = [
    new Student('Matt', 'Maths'),
    new Student('John', 'Python')
];


function enrollStudent(newStudent){
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            if(classArray){
                classArray.push(newStudent);
                console.log('student Enrolled');         
                resolve();
            }
            else{
                reject();
            }
        },2000);
        
    });
};

function getClass(){
    console.log('fetching Students');
    setTimeout(()=>{
        classArray.forEach((StudentObj)=>{
            let student = StudentObj;
            console.log(`student Name: ${student.name}, subject: ${student.subject}`)
        })
    }, 3000)
}

let newStudent = new Student('Jack', 'Javascript');
enrollStudent(newStudent)
.then(()=>getClass())
.catch(()=>console.log('Error Occured'));








//* Promise.all()

// function promise(){
//     return new Promise((resolve, reject)=>{
//         setTimeout(()=>{
//             let obj = {
//                 name:'Matt',
//                 age: 25,
//             }
//             resolve(`Name:${obj.name}, Age:${obj.age}`);

//         }, 1000)
//     });
// };

// let getPersonJob = ()=>{
//     return new Promise((resolve, reject)=>{
//         setTimeout(()=>{
//             resolve('Teacher');
//         },2000); 
//     })    
// };

// Promise.race([[promise(), getPersonJob()]]).then(()=>console.log('Only one promise complete'))


// setTimeout(()=>console.log('set TimeOut run'),1000);
// promise().then((data)=>console.log(data));

// Promise.all([promise(), getPersonJob()])
// .then((data)=>{
//     // console.log(data);             // [ 'Name:Matt, Age:25', 'Teacher' ]
//     console.log(`${data[0]}, Job:${data[1]}`)
// });


// getPersonJob().then((data)=>console.log(data));









//* Promise.all in rejected case


function promise(){
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            let obj = {
                name:'Matt',
                age: 25,
            }
            resolve(`Name:${obj.name}, Age:${obj.age}`);

        }, 1000)
    });
};

let getPersonJob = ()=>{
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            if(!true){
                resolve('Teacher');
            }
            else{
                reject('error');
            }
        },2000); 
    })
    
}; 

setTimeout(()=>console.log('set TimeOut run'),1000);
promise().then((data)=>console.log(data));

Promise.all([promise(), getPersonJob()]).then(()=>console.log('All promises Completed'))
.catch(()=>console.log('promise got rejected'));

getPersonJob().then((data)=>console.log(data))
.catch((data)=>console.log(data));               // error