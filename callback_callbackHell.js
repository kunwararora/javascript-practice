setTimeout(() => {
    console.log('timer function');
}, 2000)

function anyfunc1(func2){
    console.log('function 1');
    func2();
};
function anyfunc2(){
    console.log('function 2');
};

anyfunc1(anyfunc2);



// function fetchData(fetchData2){
//     setTimeout(() => {
//         console.log('Data 1 fetched');
//     }, 3000);
//     fetchData2();     // calling this 'step 2' function outside the asyn function leads strange behavior
// };
// function fetchData2(){
//     console.log('Data 2 Fetched');
// }
// fetchData(fetchData2);


// function fetchData(fetchData2){
//     setTimeout(() => {
//         console.log('Data 1 fetched');
//     }, 3000);
//     fetchData2();
// };
// function fetchData2(){
//     console.log('Data 2 Fetched');
// }
// fetchData(fetchData2);



//* CallbackHell (Callback pyramid)
function fetchData(fetchData2){
    setTimeout(() => {
        console.log('Data 1 fetched');
        fetchData2();
    }, 3000);
};

function fetchData2(){
    console.log('Data 2 Fetched');
    
    setTimeout(() => {
        console.log('Data 3 Fetched');

        setTimeout(() => {
            console.log('Data 4 Fetched');

            setTimeout(() => {
                console.log('Final Data is Fetched')
            }, 2000)
        }, 2000)
    }, 2000)
}

fetchData(fetchData2);