1) Take a couple of days to go over what we learnt today
2) Do watch the git video
    (
        git is super important because if you do not understand what/how to commit, merge, pull, push, resolve conflicts etc
        --> you won't be able to survive at PROVISO
    )
3) make sure you keep doing IELTS speaking
4) from now onwards spend time in learning ES6 only.
5) No day should pass without touching Javascript (only ES6)
6) We will start off with asynchronous programming whenever you feel familiar with Javascript.
7) If you need more time with Javascript you can have that we have no issues.
8) But when you feel the need to move forward we will give you the pluralsight credentials.


The ROAD forward is like this:

 - asynchronous programming in Javascript
 - angularJS
 - nodeJS
 - database concepts

You should build a full fledged dummy application in angularJS, nodeJS, postgresql
before we can schedule your actual interview.

I am targetting 7-10 February for learning everything
The next 15-20 days should be spent on building the application. (i.e by end of Feb you should be ready with the application)

We can have your interview scheduled in the first week of March.

You have more than 2 months to work on your communication skills.
We want to see some visible improvement in you within this time period.