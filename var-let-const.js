// Var 
// variable declared by 'var' will be registered as global varriable, can be access globally
var a = 10;
console.log("old value of 'a'", a)
if(a === 10){
    console.log("value of 'a' inside any scope", a)
   var a = 20;
}
console.log("new value of 'a'", a);


// let
// variable declared by 'let' will be registered as local varriable, can't be access outside that local scope
let b = 100;
if(b === 100){
    // console.log('value of b before change', b)   //can't  use b inside another scope
   let b = 50;
    console.log("value of 'b' inside", b);
}
console.log("value of 'b' outside", b);


// const
// same as let but can't be reassigned 
const c = 500;
if(c === 500){
//    c = 50;    //will throw error
    console.log('value of b inside', c);
}
console.log('value of b outside', c);