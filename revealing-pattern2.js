var displayPerson = (function(){
    // private variable
    let person = {
        name: ''
    };

    function setPerson(name){
        person.name = name;
    }
    // private function
    function setMarks(subjectName, marks){
        console.log(`${person.name} scored ${marks} in ${subjectName}`)
    };

    // return only those function which you want to expose to public with more specific naming scheme
    return{
        setPerson: setPerson,
        setMarks: setMarks
    };
})();
displayPerson.setPerson('John')
displayPerson.setMarks('English', 50);
