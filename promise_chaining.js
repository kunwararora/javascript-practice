
//* with callbacks(which will leads to callback pyramid)

// let data = [
//     {
//         orderName: 'mobile Phone',
//         orderPrice: 12000,
//         address: 'ludhiana',
//         orderStatus: 'Shipped',
//     }
// ];


// let showOrder = ()=>{
//     setTimeout(()=>{
//         console.log(`You have ${data.length} order`);

//         setTimeout(()=>{
//             console.log(`Name of the product: ${data[0].orderName}`);
            
//             setTimeout(() => {
//                 console.log(`will be delivered in ${data[0].address}`);

//                 setTimeout(()=>{
//                     console.log(`Order Status: ${data[0].orderStatus}`);

//                     setTimeout(()=>{
//                         console.log(`Order Delivered`);
//                     }, 3000);
//                 },2000);
//             },0000);
//         },2000);
//     },2000);
// };
// showOrder();




//* with Promises

let data = [
    {
        orderName: 'mobile Phone',
        orderPrice: 12000,
        address: 'ludhiana',
        orderStatus: 'Shipped',
    }
];

let showOrder = (timer, func) => {
    return new Promise((resolve, reject) => {
        if(data){
            setTimeout(()=>{
                resolve( func());
            }, timer);
        }
        else{
            reject();
        };
    });
};

showOrder(2000,()=> console.log(`You have ${data.length} order`))
.then(()=>{
    return showOrder(2000,()=> console.log(`Name of the product: ${data[0].orderName}`))
})
.then(()=>{
    return showOrder(0000,()=> console.log(`will be delivered in ${data[0].address}`))
})
.then(()=>{
    return showOrder(2000,()=>  console.log(`Order Status: ${data[0].orderStatus}`))
})
.then(()=>{
    return showOrder(2000,()=> console.log(`Order Delivered`))
})

.catch(()=>console.log(`Can't fetch the Data`));





// let getrollNumber = new Promise((resolve, reject)=>{
//     let rollNumber = [1,2,3,4,5];
//     if(rollNumber){
//         resolve(rollNumber)
//     }
//     else{
//         reject(`Couldn't fetch the Roll numbers`)
//     }
// });

// function getStudentInfo(rollNumbers){
//     return new Promise((resolve, reject)=>{
//         setTimeout((rollNumbers)=>{
//             if(rollNumbers){
//                 let student = {
//                     name:'John',
//                     age: 21,
//                     subject:'Python',
//                 };
//                 let getInfo = ()=>{
//                     return `My name is ${student.name}, My roll number is ${rollNumbers[1]}, I am ${student.age} years old`
//                 };

//                 resolve(getInfo());
//             }
//         },2000, rollNumbers)
//     })
// }

// getrollNumber.then((data)=>{
//     console.log('Roll number fetched')
//     console.log(data)
//     return getStudentInfo(data);
// })
// .then((studentInfo)=>{
//     console.log(studentInfo);
// })
// .catch(()=>console.log('Error')) ;