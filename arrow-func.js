//* normal function
function add(a , b){
    return a + b;
};
console.log(add(5, 10));

var obj = {
    name:'John',
    age: 25,
    getInfo: function(){console.log('Name:' + this.name + ' age: ' + this.age)}
};
obj.getInfo();





//* Arrow function

// Arrow functions cant use as methods

// var add2 = (a, b) => {
//     return a + b;
// };
let add2 = (a, b) => a + b;   // no need to use 'return' if function is of one line of code and does't contain any {}
console.log(add2(10, 15));


var obj2 = {
    name:'Matt',
    age: 25,

    // you cant use 'this' in arrow function nested to an object becoz 'this' points in arrow function to windows
    getInfo: () => {console.log('Name:' + this.name + ' age: ' + this.age)}    // undefined
};
obj2.getInfo();