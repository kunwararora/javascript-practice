// var array1 = [];
// var array2 = [1,2,3,4,5];

// // ES5
// array2.forEach(




var newPromise = function newPromise(message) {
    return new Promise(function (resolve, reject) {
        if (!message) {
        reject(new Error('No Message'));
        }
    
        resolve(message);
    })
};

async function newFunction(){
    var func = await newPromise('Hello');
    console.log(func);
};
newFunction();