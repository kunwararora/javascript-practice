// async function anyfunc(){
//     setTimeout(()=>console.log('timer'),2000);
//     return 'Hello';
// };
// anyfunc().then((data)=>console.log(data));



// async function anyfunc(){
//     await setTimeout(()=>console.log('timer'),2000);
//     return 'Hello';
// };
// // console.log(anyfunc());         // returns promise
// anyfunc().then((data)=>console.log(data));




function getPeople(){
    return new Promise(resolve=>{
        setTimeout(()=>resolve(['jack','Matt','Jim']),2000);
    })
}
function getPerson(people,a){
    return new Promise(resolve=>{
        setTimeout(()=>resolve(people[a]),3000);
    })
};

async function anyfunc(){
    const a = await getPeople();
    const b = await getPerson(a,1); 
    console.log(a);
    console.log(b);
   
};
anyfunc();

async function anyfunc(){
    const a = await getPeople();
    const b = await getPerson(a,1); 
    console.log(a);
    console.log(b);

    const c = 'random data returning';
    return c;
   
};
anyfunc()
.then(data=>console.log(data));







// promise chaining & async wait 


// Doing with promise chaining
let showOrders = (timer,work)=>{    
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            let data = [
                {
                    orderName: 'mobile Phone',
                    orderPrice: 12000,
                    address: 'ludhiana',
                    orderStatus: 'Shipped',
                },
                {
                    orderName: 'Laptop',
                    orderPrice: 22000,
                    address: 'ludhiana',
                    orderStatus: 'Out for Delivery',
                }
            ];
            try{
                if(work === "getData"){
                    resolve(data);
                }
                else{
                    resolve(work());
                }

            }
            catch(error){
                reject(console.log('Data not Found'));
            };
        },timer);
    });
};

// showOrders(2000, "getData")
// .then((data)=>{
//     orders = data;

//     return showOrders(2000, ()=>console.log(orders));
// })
// .then(()=>{
//     return showOrders(2000,()=>console.log(`You Have ${orders.length} orders`));
// })
// .then(()=>{
//     return showOrders(2000,()=>console.log(`Name of the first Product: ${orders[0].orderName}`));
// })
// .then(()=>{
//     return showOrders(1000,()=>console.log(`Order Status: ${orders[0].orderStatus}`));
// })
// .then(()=>{
//     return showOrders(2000,()=>console.log(`Name of the second Product: ${orders[1].orderName}`));
// })
// .catch(()=>console.log("Error Occured inside any call"))
// .finally(()=>console.log('All Done'));





// Doing with Async await
async function logOrder(){
    try{
        const a = await showOrders(2000, "getData");
        const b = await showOrders(2000, ()=>console.log(a));
        const c = await showOrders(2000,()=>console.log(`You Have ${a.length} orders`));

        // Creating custom error
        // const g = await showOrders(2000,()=>{throw 'Custom Error'});

        const d = await showOrders(2000,()=>console.log(`Name of the first Product: ${a[0].orderName}`));
        const e = await showOrders(2000,()=>console.log(`Order Status: ${a[0].orderStatus}`));
        const f = await showOrders(1000,()=>console.log(`Name of the second Product: ${a[1].orderName}`))
    }
    catch(error){
        console.log('error Occured')
    }
};
logOrder().finally(()=>console.log('All Done'));