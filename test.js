function sayHello(){
    const name = 'John';
    return function sayJohn(){
        console.log('Hello ' + name);
    };
}


var name2 = 'Mathew';
if(true){
    var name2 = 'Matt'; 
    // let name2 = 'Matt'; 
};
console.log(name2);


// console.log(a);
const a = 10;


var name3 = 'Matt';
if(true){
    console.log(name3);
    // const name3 = 'John';
}



// console.log(this)
// function anyFunc(){
//     console.log(this);
// }
// anyFunc();


const obj = {
    name: 'jack',
    anyfunc(){
        console.log(this);
    }
}
// obj.anyfunc();

let b = obj.anyfunc;
// b();

// #######
const obj2 = {
    anyfunc(){
        return function anyfunc2(){
            console.log(this);
        }
    }
};
// obj2.anyfunc()();

const obj3 = {
    anyfunc(){
        return () => {
            console.log(this);
        }
    }
};
// obj3.anyfunc()();




var displayPerson = function(){
    let person = {
        name: 'Jack',
        rollNumber: 45 
    };
    function setMarks(subjectName, marks){
        console.log(`${person.name} scored ${marks} in ${subjectName}`)
    };
    return{
        setMarks: setMarks,

        getRollNumber: function getRollNumber(){
            console.log(person.rollNumber);

            // let name = person.name;
            // return name;
        }
    };
};
var mainFunc = displayPerson()

// console.log(mainFunc);
// console.log(mainFunc.setMarks);
// console.log(mainFunc.getRollNumber())

console.log(mainFunc.person);  
mainFunc.setMarks('English', 50); 
mainFunc.getRollNumber() 